#! /usr/bin/env python3

from flask import Flask, render_template
app = Flask(name)
app.debug = True

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    dico={"liste": ["rien", "pas grand chose", "sieste"]}
    return render_template(
        "user.html",
        name=name,
        todo=dico["liste"])

if name == 'main':
    app.run(host="0.0.0.0")
